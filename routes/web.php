<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/abc', function () {
    return view('welcome');
});

// Route::get('/',function(){
// 	return view('abc');
// });

Auth::routes(['verify'=>true]);
Route::get('/','practiceController@index');
Auth::routes();



// Route::get('/dashboard','HomeController@create');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/addproduct','HomeController@addproduct');

Route::post('/addproduct','productController@store');

Route::get('dropdownlist/getsubcategories/{id}','HomeController@getsub');

Route::get('/addcategories','categories@addcategories');

Route::post('/addcategories','categories@savecategories');

Route::post('/abcd','categories@abcd');

Route::get('/showproducts','productController@index');

Route::get('updateproducts/{id}','productController@update');

Route::post('updateproducts/{id}','productController@edit');

Route::get('/product/details/{slug}','productController@showdetails');

Route::get('/single/product/{id}','CartController@addtocart');

Route::get('product/delete/{id}','CartController@deletefromcart');

Route::get('/viewcart','CartController@viewcart');

Route::get('/viewcart/{code}','CartController@viewcart'); //jodi ajax diye kori thole lagto

Route::post('/updatecartquantity','CartController@updatecartquantity');

Route::post('/abcde','CartController@abir');

Route::get('/checkout','HomeController@checkout');

Route::get('shipping/getstates/{id}','HomeController@getstates');

Route::get('shipping/getcity/{id}','HomeController@getcity');

Route::post('shippinginfo','checkoutController@saveshipping');

// Route::get('/stripe', 'StripePaymentController@stripe');

Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
// Route::group(['middleware' =>'auth'], function(){
// 	Route::get('/laravel-filemanager','\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
// 	Route::post('/laravel-filemanager/upload','UniSharp/LaravelFilemanager/Controllers/uploadController@upload');

// });

Route::get('/loggedout','practiceController@sessionout')->name('/loggedout');

// Route::group(['middleware' => 'App\Http\Middleware\simpleMiddleware'], function()
//     {
//       	Route::get('/addproduct','HomeController@addproduct');

//     });

// Route::group(['middleware' => 'App\Http\Middleware\dashboardthroughmanage'], function()
//     {
//       	Route::get('/home', 'HomeController@index')->name('home');
//       	//dashboard er tau asbe 
//     });

