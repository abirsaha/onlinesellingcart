<?php

namespace App\Http\Middleware;

use Closure;

class dashboardthroughmanage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if($request->user() && $request->user()->user_role == "admin"){
        return redirect('/dashboard1');
           
        }
        
         return $next($request);
    }
}
