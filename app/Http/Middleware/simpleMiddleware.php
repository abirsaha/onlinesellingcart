<?php

namespace App\Http\Middleware;

use Closure;

class simpleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() && $request->user()->user_role == "admin"){
        return redirect('/dashboard1');
           
        }
        else if($request->user() && $request->user()->user_role == "user"){
        return redirect('/dashboard');
           
        }
        else{
         return redirect('/login'); 
        }
    }
}
