<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\cart;
use App\cupon;
use DB;

class CartController extends Controller
{
    
	public function addtocart($id,Request $request){
		
		$mac = exec('getmac');
		$mac = strtok($mac,' ');

		$cart = cart::where('product_id',$id)->where('device',$mac)->count();

		if($cart>0){
			cart::where('product_id',$id)->where('device',$mac)->increment('cart_quantity',1);
		}
		else{
			cart::Insert([
				'product_id'=> $id,
				'cart_quantity'=> 1,
				'device'=> $mac
			]);

		}

		return back();
	}


	public function deletefromcart($id){
		DB::table('carts')
		->where('cart_id', $id)
		->delete();
		return back();
	}

	public function viewcart($code=' '){
		
		if($code==' '){
		$mac = exec('getmac');
        $mac = strtok($mac,' ');
        $value = DB::table('products')
                ->join('carts','products.id','carts.product_id')
                ->where('device',$mac)
                ->get();
              
       $values = $value;
       $values2=$value;

       $cartitems = DB::table('carts')
					->select(DB::raw("SUM(p_price * cart_quantity) as totalcost"),'cart_id','p_name','cart_quantity')
					->join('products','products.id','carts.product_id')
					->groupby('cart_id','p_name','cart_quantity')
					->get();

		$total = 0;

		foreach ($cartitems as $cartitems) {
						$total = $total + $cartitems->totalcost;
					}

	    $tax = ($total*4)/100;
	    $grandtotal = $total + $tax;

	    session(['total'=>$total, 'tax'=>$tax , 'grandtotal'=>$grandtotal]);


      
		return view('fontEnd.cart.viewcart',compact('value','values','values2'));
	}

	else{
		$cupon="dhaka276";

		$mac = exec('getmac');
        $mac = strtok($mac,' ');
        $value = DB::table('products')
                ->join('carts','products.id','carts.product_id')
                ->where('device',$mac)
                ->get();
              
       $values = $value;
       $values2=$value;
      
	  return view('fontEnd.cart.viewcart1',compact('value','values2','values','cupon')); 
      
		

		}


}


   	public function applycode(Request $request){
   		// echo $request->cuponcode;
   		//cart::where('cupon_code',$request->cuponcode)->exists();

   		$mac = exec('getmac');
        $mac = strtok($mac,' ');
        $value = DB::table('products')
                ->join('carts','products.id','carts.product_id')
                ->where('device',$mac)
                ->get();
              
       $values = $value;
       $values2=$value;
      
	  return view('fontEnd.cart.viewcart',compact('value','values2','values')); 
   	}


	public function updatecartquantity(Request $request){
		foreach ($request->id as $key => $value) { 
			DB::table('carts')
            ->where('cart_id', $value)   //$value hold the cart's cart_id here
            ->update(['cart_quantity' => $request->quantity[$key]]);
		}




		 return back();
	}

	public function abir(Request $request){
		echo $request->cuponcode;
	}

	



}
