<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use App\product;
use Illuminate\Support\Str;
use DB;
class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function __construct()
    {
        $this->middleware('auth');
         $this->middleware('verified');
    }


    public function index()
    {
        //

        $products =  DB::table('products')
                    ->select('*')
                    ->join('categories','products.category_id','categories.c_id')
                    ->join('subcategories','products.sub_id','subcategories.s_id')
                    ->get();
        
        return view('fontEnd.shows.showproducts')->with(compact('products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('p_image')){
        $get_image = $request->file('p_image');
        $image = time().Str::random(10).'.'.$get_image->getClientOriginalExtension();
        Image::make($get_image)->resize(300,200)->save(public_path('images/product/'.$image));
    }

        else{
            $image = "default.png";
        }

        product::insert([
            'p_name' => $request->p_name,
            'p_price' => $request->p_price,
            'description' =>$request->description,
            'p_image' => 'images/product/'.$image,
            'quantity'=>$request->quantity,
            'alert_quantity' => $request->alert_quantity,
            'category_id' => $request->country,
            'sub_id' => $request->state,
            'slug' =>$request->slug
        ]);

        return redirect('/showproducts');



                            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
         $id = $request->session()->get('p_id');
        $product_image=product::findorFail($id)->p_image;
        $product_cat = product::findorFail($id)->category_id;
         $product_subcat =product::findorFail($id)->sub_id;
         if($request->hasFile('p_image')){
        $get_image = $request->file('p_image');
        $image = time().Str::random(10).'.'.$get_image->getClientOriginalExtension();
        Image::make($get_image)->resize(300,200)->save(public_path('images/product/'.$image));

        if(file_exists($product_image)){
            unlink($product_image);
        }
    }

    else{
        $image = $product_image; 
    }

    product::findorFail($id)->update([
            'p_name' => $request->p_name,
            'p_price' => $request->p_price,
            'alert_quantity' => $request->alert_quantity,
            'description' =>$request->description,
            'p_image' => 'images/product/'.$image,
            'category_id' => $product_cat,
            'sub_id' => $product_subcat
        ]);
  


        return redirect('/showproducts');

        // echo $request->category;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $products=DB::table('products')
            ->select('*')
            ->join('categories','products.category_id','categories.c_id')
            ->join('subcategories','products.sub_id','subcategories.s_id')
            ->where('id',$id)
            ->get();
            session(['p_id'=>$id]);

       return view('fontEnd.updateform.productupdate',['products'=>$products]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showdetails($slug){

        $product = DB::table('products')
            ->select('*')
            ->join('subcategories','products.sub_id','subcategories.s_id')
            ->join('categories','products.category_id','categories.c_id')
             ->where('slug',$slug)
            ->get();

       
            
       $mac = exec('getmac');
        $mac = strtok($mac,' ');
        $value = DB::table('products')
                ->join('carts','products.id','carts.product_id')
                ->where('device',$mac)
                ->get(); 

       $newproduct = DB::table('products')
                  ->select('*')
                  ->latest()
                  ->limit(8)
                  ->get();
return view('fontEnd.shows.productdetails',compact('product','value','newproduct')); 
    }

     
}
