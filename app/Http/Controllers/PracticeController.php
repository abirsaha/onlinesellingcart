<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\product;

class PracticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $product = product::inRandomOrder()->get();
        $product =DB::table('products')
                  ->select('*')
                  ->latest()
                  ->limit(8)
                  ->get();

        $mac = exec('getmac');
        $mac = strtok($mac,' ');
        $value = DB::table('products')
                ->join('carts','products.id','carts.product_id')
                ->where('device',$mac)

                ->get();

         $randomproduct = product::all()->random(8);
         $values8 = $value;

         $toprated =  DB::table('products')
                      ->orderBy('p_price','DESC')
                      ->limit(3)
                      ->get();
              
        return view('fontEnd.homepage',compact('product','value','randomproduct','values8','toprated'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sessionout(){
        session_stop();
    }
}
