<?php

namespace App\Http\Controllers;
use App\category;
use App\subcategory;

use Illuminate\Http\Request;

class categories extends Controller
{
 public function __construct()
    {
        $this->middleware('verified');
    }    

    public function addcategories(){
        $category = category::all();
        // echo "<pre>";
        // print_r($category);
    	return view('fontEnd.dynamicpages.addandshowcategories',compact('category'));
    }

    public function savecategories(Request $request){
    	//category::create($request->all());
    	category::create([
    		'cat_name'=>$request->c_name,
    	]);
        return back();
    }

    public function abcd(Request $request){
    	subcategory::create([
            'c_id'=>$request->c_id,
            'sub_name'=>$request->sub_name,
        ]);

       return back();
            }
}
