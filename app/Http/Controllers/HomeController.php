<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');  //for user
    }

    // public function create(){
    //     $countries =  DB::table('categories')->pluck("cat_name","c_id");
        
    //     return view('fontEnd.forms.addproduct',compact('countries'));
    // }

    public function addproduct(){
         $countries =  DB::table('categories')->pluck("cat_name","c_id");
        
        return view('fontEnd.forms.addproduct',compact('countries'));
    }

    public function addproduct1(Request $request){
        product::create([
            'p_name'=>$request->p_name,
            'p_price'=>$request->p_price,
            'description'=>$request->description,
        ]);

       //  echo "<script>window.alert('done')</script>";
       // // return redirect('/addproduct');
       //      echo "<script>window.open('localhost/onlinesellingcart/public/addproduct')</script>";


    //  
    }

      public function getsub($id){
       $states = DB::table("subcategories")->where("c_id",$id)->pluck("sub_name","s_id");
        return json_encode($states);
    }

    public function checkout(){
    

        $cartitems = DB::table('carts')
                    ->select(DB::raw("SUM(p_price * cart_quantity) as totalcost"),'cart_id','p_name','cart_quantity')
                    ->join('products','products.id','carts.product_id')
                    ->groupby('cart_id','p_name','cart_quantity')
                    ->get();

        $total = 0;

        foreach ($cartitems as $cartitem) {
                        $total = $total + $cartitem->totalcost;
                    }

        $tax = ($total*4)/100;
        $grandtotal = $total + $tax;

        $countries= DB::table('countries')
                    ->select('*')
                    ->get();

           session(['total'=>$total, 'tax'=>$tax , 'grandtotal'=>$grandtotal]);

        

        return view('fontEnd.checkout.checkout',compact('cartitems','total','tax','grandtotal','countries'));   
}

public function getstates($id){
    $states = DB::table("states")->where("country_id",$id)->pluck("name","id");
        return json_encode($states);
}


public function getcity($id){
    $cities = DB::table('cities')
              ->where('state_id',$id)
              ->pluck("name","id");
    return json_encode($cities);          
}

}
