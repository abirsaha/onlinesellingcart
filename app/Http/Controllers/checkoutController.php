<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\shipping;
use App\sale;
use App\billing;
use App\product;
use App\cart;
use Auth;
use Session;
use Stripe;
use App\Mail\ordermail;
use Illuminate\Support\Facades\Mail;
use PDF;
class checkoutController extends Controller
{
    //
	




public function saveshipping(Request $request){
	$shipping_id = shipping::insertGetId([
		'f_name'=>$request->f_name,
		'l_name'=>$request->l_name,
		'user_id'=>Auth::id(),
		'company_name'=>$request->c_name,
		'street_address'=>$request->street_address,
		'house_no'=>$request->apartment,
		'country_id'=>$request->country,
		'state_id'=>$request->state,
		'city_id'=>$request->city,
		'post_code'=>$request->zip,
		'email_adress'=>$request->email,
		'phone_num'=>$request->phone,
		'payment_status'=>"unpaid",
	]);

	$grandtotal =  session()->get('grandtotal')	;

	$sale_id = sale::insertGetId([
		'user_id'=>Auth::id(),
		'shipping_id'=>$shipping_id,
		'grand_total'=> $grandtotal,
	]);



	\Stripe\Stripe::setApiKey('sk_test_o55HEktTVBqQbDzBNPeOlNDQ00mitnBVJE');
      $Charges =  \Stripe\Charge::create ([
                "amount" => 100 * $grandtotal,
                "currency" => "usd",
                "source" => "tok_visa",
                "description" => "Test payment from itsolutionstuff.com." 
        ]);

     shipping::findorFail($shipping_id)->update(['payment_status' => 'paid']);
  
       Session::flash('success', 'Payment successful!');

       $mac = exec('getmac');
        $mac = strtok($mac,' ');
        $value = DB::table('products')
                ->join('carts','products.id','carts.product_id')
                ->where('device',$mac)
                ->get();

       foreach ($value as  $value) {
          
            billing::insert([
       			'sale_id'=>$sale_id,
				  'user_id'=>Auth::id(),
				  'product_id'=> $value->product_id,
				  'product_quantity'=>$value->cart_quantity,
				  'product_price'=> product::findorFail($value->product_id)->p_price,
				  'payment_type'=> $Charges->source->brand,				
       				]);

             product::findorFail($value->product_id)->decrement('quantity',$value->cart_quantity);
           
            cart::where('product_id',$value->product_id)->delete();   //after the payment the cart will delete one by one 
            



}
   			
		$data = DB::table('billings')
              ->join('sales','sales.id','billings.sale_id')
              ->join('products','billings.product_id','products.id')
              ->where('sale_id',$sale_id)
              ->select('*')
              ->get();
    // $data1 = DB::table('products')
    //             ->join('carts','products.id','carts.product_id')
    //             ->where('device',$mac)
    //             ->count();    

            // Mail::to('sahaaabir917@gmail.com',new ordermail($data));
       //     Mail::to('sahaaabir917@gmail.com',new ordermail($data));
      // Mail::to("sahaaabir917@gmail.com")->send(new ordermail($data));
        
      $data2 =$data;
       session(['shipping_id'=>$shipping_id, 'sale_id'=>$sale_id]); 
			$pdf = PDF::loadView('fontEnd.mailpage.order', compact('data','data2'));
        
            
        try{
            Mail::send('fontEnd.mailpage.order', compact('data','data2'), function($message)use($data,$data2,$pdf) {
            $message->to(Auth::user()->email)
            ->subject("your order")
            ->attachData($pdf->output(), "invoice.pdf");
            });
        }catch(JWTException $exception){
            $this->serverstatuscode = "0";
            $this->serverstatusdes = $exception->getMessage();
        }




          return back();


}

}
