<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    //
    protected $fillable = [
        'p_name', 'p_price', 'description','p_image','alert_quantity','category_id','sub_id','quantity','slug',
    ];
}
