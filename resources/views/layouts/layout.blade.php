@extends('layouts.layout')

@section('header')
                <div class="shopping__cart">
                <div class="shopping__cart__inner">
                    <div class="offsetmenu__close__btn">
                        <a href="#"><i class="zmdi zmdi-close"></i></a>
                    </div>
                    <div class="shp__cart__wrap">
                       @foreach($value as $value)
                        <div class="shp__single__product">
                            <div class="shp__pro__thumb">
                                <a href="{{url('product/details')}}/{{$value->slug}}">
                                    <img src="{{$value->p_image}}" alt="product images">
                                </a>
                            </div>
                            <div class="shp__pro__details">
                                <h2><a href="{{url('product/details')}}/{{$value->slug}}">Product Name ab :  {{$value->p_name}}</a></h2>
                                <span class="quantity">Quantity : {{$value->cart_quantity}}</span>
                                <span class="shp__price">Price : {{$value->p_price}}</span>
                            </div>
                            <div class="remove__btn">
                                <a href="{{url('product/delete')}}/{{$value->cart_id}}" title="Remove this item"><i class="zmdi zmdi-close"></i></a>
                            </div>
                        </div>
                       @endforeach
                    </div>
                    <ul class="shoping__total">
                        <li class="subtotal">Subtotal:</li>
                        <li class="total__price">$130.00</li>
                    </ul>
                    <ul class="shopping__btn">
                        <li><a href="cart.html">View Cart</a></li>
                        <li class="shp__checkout"><a href="checkout.html">Checkout</a></li>
                    </ul>
                </div>
            </div>
@endsection