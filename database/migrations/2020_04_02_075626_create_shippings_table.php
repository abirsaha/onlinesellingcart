<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('f_name');
            $table->string('l_name');
            $table->unsignedBigInteger('user_id');
            $table->string('company_name')->nullable();
            $table->string('street_address');
            $table->string('house_no');
            $table->unsignedBigInteger('country_id')->nullable;
            $table->unsignedBigInteger('state_id')->nullable;
            $table->unsignedBigInteger('city_id')->nullable;
            $table->string('post_code');
            $table->string('email_adress');
            $table->string('phone_num');
            $table->string('payment_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shippings');
    }
}
