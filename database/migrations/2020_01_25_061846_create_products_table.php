<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('p_name');
            $table->string('slug');
            $table->integer('p_price');
            $table->string('description',10000);
            $table->string('p_image');
            $table->Integer('quantity');
            $table->Integer('alert_quantity');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('c_id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('sub_id');
            $table->foreign('sub_id')->references('s_id')->on('subcategories')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
