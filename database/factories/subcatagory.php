<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\category;
use App\subcategory;
$factory->define(App\Model\subcategory::class, function (Faker $faker) {
    return [
        //
        'sub_name'=> $faker->word(),
		'c_id'=> function(){
			return category::all();
		},
    ];
});
