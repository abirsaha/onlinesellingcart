<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\category;
$factory->define(App\Model\category::class, function (Faker $faker) {
    return [
        'cat_name'=>$faker->word(),
    ];
});
